cmake_minimum_required(VERSION 2.8)
project(unbuffered-channel)

if (TEST_SOLUTION)
  include_directories(../private/unbuffered-channel/ok)
endif()

include(../common.cmake)

add_gtest(test_unbuffered_channel test.cpp)
add_benchmark(bench_unbuffered_channel bench.cpp)
